# DONE
- Program dari skripsi sudah selesai
- Rework admin client menggunakan ReactJS (I know i can do it!)
- Migrasi DB ke mongo, jadi lebih mudah akses dan managementnya (You are Great!)
- Scheduler dari parsehub juga udah work (Great! thanks to gitlab CI/CD)
- Scheduler untuk rates (Belum dites)
- Puppeter untuk search ke Shopee dan Amazon juga udah work dalam microservice terpisah (Great!)
- Total terkumpul data menggunakan scraping ada += 100
    - 29 Huion (16 Pen Tablet, 13 Pen Display)
    - 38 XP Pen (24 Pen Tablet, 14 Pen Display)
    - ? Gaomon (? Pen Tablet, ? Pen Display)
    - ? Wacom (? Pen Tablet, ? Pen Display)
- Update skema db pada penulisan
- Update flowchart pada penulisan


# TODO
- Resolve fragment bug
- Revisi dan translate Bab 1
    - Revisi batasan masalah
    - Revisi tujuan
    - Riset kembali evaluasi fuzzy tahani
- Revisi bab 2
    - Update library yang digunakan
- Revisi bab 3
    - Cek kembali kesesuaian dari himpunan fuzzy
    - Update analisa fuzzy tahani
    - Update perancangan antarmuka
- Revisi bab 4
    - Update uji coba blackbox
- Laporan dan Bimbingan,
    - laporan dengan jelas, berapa data yang terkumpul, bagaimana mekanisme realtime nya
    - tanyakan dengan jelas apa saja yang perlu dijadikan evaluasi dan uji coba.Jika pada akhirnya harus menyebar kuesioner tanyakan 
        - berapa minimal dan maksimal responden.    
        - konteks pertanyaan ini apa? fungsional programnya kah? atau apa?
        - berapa jumlah pertanyaan yang harus disiapkan?
    - Jika pada akhirnya menggunakan responden, coba hubungi teman yang punya interest di desain grafis
        - Fadhel
        - Abu
        - Om Daniel
        - Gilang
        - Mungkin coba tanya ke reza, siapa tau dia mau bantu        
- Agenda Bimbingan

# JANGAN DOWN, LU PASTI BISA
# TADINYA LU GABISA BUAT REALTIME TAPI NYATANYA SEKARANG BISA, KENAPA EVALUASI JUGA GAK BISA???
# INGET MENUNDA SKRIPSI = MENUNDA LIBURAN, BELAJAR DESAIN, DAN OPEN CV
